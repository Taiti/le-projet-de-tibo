#I. Installation d'un serveur DHCP

```bash
sudo dnf install dhcp-server
```

```bash
sudo nano /etc/dhcp/dhcpd.conf

default-lease-time 600;
max-lease-time 7200;
authoritative;

# PXE specifics
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.1.1.0 netmask 255.255.255.0 {
    # définition de la range pour que votre DHCP attribue des IP entre <FIRST_IP> <LAST_IP>
    range dynamic-bootp 10.1.1.10 10.1.1.50;

    # add follows
    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 10.1.1.101;

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
 	 }
    }
}
```
```bash
sudo systemctl enable dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
[root@node1 vagrant]# sudo systemctl status dhcpd
○ dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: inactive (dead)
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)


 sudo firewall-cmd --add-service=dhcp --permanent
success
 sudo firewall-cmd --reload
success
```

#II. Installation d'un serveur TFTP


```bash
Installed:
  tftp-server-5.2-37.el9.x86_64

Complete!
```

```bash
sudo systemctl enable --now tftp.socket
Created symlink /etc/systemd/system/sockets.target.wants/tftp.socket → /usr/lib/systemd/system/tftp.socket.
```

```bash
sudo firewall-cmd --add-service=tftp --permanent
success
sudo firewall-cmd --reload	
success
```

#IV. Installation d'un serveur Apache

```bash
sudo systemctl status httpd
○ httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; preset: disabled)
     Active: inactive (dead)
       Docs: man:httpd.service(8)
```


```bash
sudo nano /etc/httpd/conf.d/pxeboot.conf

Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    # access permission
    Require ip 127.0.0.1 10.1.1.0/24 # remplace 10.1.1.0/24 par le réseau dans lequel se trouve le serveur
</Directory>
```


```bash
sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Thu 2024-04-04 13:23:18 UTC; 2min 45s ago
```


```bash
sudo firewall-cmd --add-port=80/tcp --permanent
success

sudo firewall-cmd --reload
success
```



