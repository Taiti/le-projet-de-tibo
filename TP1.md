# I. Une première VM
## 1. ez startup
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
```

## 2. Un peu de conf
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11", netmask: "24"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei" do |h|
    h.vm.disk :disk, size: "20GB", primary: true
  end
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end
end
```


# II. Initialization script

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11", netmask: "24"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei" do |h|
    h.vm.disk :disk, size: "20GB", primary: true
  end
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end
  config.vm.provision "shell", path: "script.sh"
end
```

```bash
script.sh
sudo dnf update -y
sudo dnf install -y vim python3
```

# III. Repackaging

```bash
$ vagrant status
Current machine states:

ezconf.tp1.efrei          running (virtualbox)
$ vagrant package --output rocky-efrei.box
$ vagrant box add rocky-efrei rocky-efrei.box
```

# IV. Multi VM

```bash
$ vagrant ssh node1.tp1.efrei
Last login: Tue Apr  2 15:49:34 2024 from 10.0.2.2
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=0.848 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=3.01 ms
```

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "rocky-efrei.box"
  config.vm.define "node1.tp1.efrei" do |node1|
    node1.vm.network "private_network", ip: "10.1.1.101", netmask: "255.255.255.0"
    node1.vm.disk :disk, size: "100GB", primary: true
    node1.vm.hostname = "node1.tp1.efrei"
    node1.vm.provider "virtualbox" do |v|
      v.memory = 2048
    end
  end
end
```
```bash
lala:x:1001:1001:Super adminsys:/home/lala:/bin/bash
[lala@node1 vagrant]$
```
