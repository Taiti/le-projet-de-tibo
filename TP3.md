#II. Setup

##1. Frontend

```bash
.database
sudo dnf install mysql-community-server.x86_64

	
● mysqld.service - MySQL Server
     Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-04-09 09:45:29 UTC; 9s ago
       Docs: man:mysqld(8)

2024-04-09T09:45:11.761432Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: uDj915oAl;oZ
```

```bash
mysql -u root -p
password: uDj915oAl;oZ

ALTER USER 'root'@'localhost' IDENTIFIED BY 'Jefaisunstrongpassword33!';
Query OK, 0 rows affected (0.01 sec)
CREATE USER 'oneadmin' IDENTIFIED BY 'Jefaisunsuperstrongpassword33!';
Query OK, 0 rows affected (0.03 sec)
CREATE DATABASE opennebula;
Query OK, 1 row affected (0.02 sec)
GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
Query OK, 0 rows affected (0.02 sec)
SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
Query OK, 0 rows affected (0.01 sec)
```

##B. OpenNebula

```bash
sudo dnf install -y opennebula opennebula-sunstone opennebula-fireedge

/etc/one/oned.conf
DB = [ BACKEND = "mysql",
       SERVER  = "localhost",
       PORT    = 0,
       USER    = "oneadmin",
       PASSWD  = "Jefaisunsuperstrongpassword33!",
       DB_NAME = "opennebula",
       CONNECTIONS = 25,
       COMPARE_BINARY = "no" ]


sudo su - oneadmin
cat /var/lib/one/.one/one_auth
oneadmin:oneadmin		


sudo systemctl start opennebula-sunstone
sudo systemctl start opennebula


[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=9869/tcp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=2633/tcp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=4124/tcp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=4124/udp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=29876/tcp
success

	

kvm

sudo dnf install -y epel-release
sudo dnf install -y opennebula-node-kvm
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
 sudo systemctl status libvirtd
● libvirtd.service - Virtualization daemon
     Loaded: loaded (/usr/lib/systemd/system/libvirtd.service; enabled; preset: disabled)
     Active: active (running) since Wed 2024-04-10 08:02:54 UTC; 45s ago


[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
success
[vagrant@rocky9 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=8472/udp
success
```
##C preparer le bridge

```bash
sudo ip link add name vxlan_bridge type bridge
sudo ip link set dev vxlan_bridge up
sudo ip addr add 10.220.220.201/24 dev vxlan_bridge
sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
success
sudo firewall-cmd --add-masquerade --permanent
success
sudo firewall-cmd --reload
success
```